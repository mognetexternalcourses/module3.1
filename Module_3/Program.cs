﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            throw new NotImplementedException();
        }

        public int Multiplication(int num1, int num2)
        {
            throw new NotImplementedException();
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            throw new NotImplementedException();
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            throw new NotImplementedException();
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            throw new NotImplementedException();
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            throw new NotImplementedException();
        }
    }
}
